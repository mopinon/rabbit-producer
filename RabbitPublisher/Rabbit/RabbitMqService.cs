using System.Text;
using System.Text.Json;
using RabbitMQ.Client;

namespace RabbitPublisher.Rabbit;

public class RabbitMqService : IRabbitMqService
{
    public void SendMessage(object obj)
    {
        var message = JsonSerializer.Serialize(obj);
        SendMessage(message);
    }

    public void SendMessage(string message)
    {
        var factory = new ConnectionFactory { HostName = "92.63.97.5" };
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare("MyQueue",
                false,
                false,
                false,
                null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish("",
                "MyQueue",
                null,
                body);
        }
    }
}